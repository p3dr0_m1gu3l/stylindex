from django.db import models


class Author(models.Model):
    name = models.CharField(max_length=250, unique=True)

    def __str__(self):
        return self.name


class Book(models.Model):
    book_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=250)
    isbn = models.CharField(max_length=16, unique=True)
    published_at = models.DateField()
    authors = models.ManyToManyField(Author)
    cover = models.URLField(max_length=200)

    def __str__(self):
        return self.name
