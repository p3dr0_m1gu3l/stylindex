from .models import Author, Book


class AuthorService:

    @staticmethod
    def get_all():
        return Author.objects.all()

    @staticmethod
    def get_or_create(data):
        return Author.objects.get_or_create(**data)

    @staticmethod
    def create(data):
        return Author.objects.create(**data)

    @staticmethod
    def delete_all():
        return Author.objects.all().delete()


class BookService:

    @staticmethod
    def get_all():
        return Book.objects.all()

    @staticmethod
    def get_or_create(data):
        return Book.objects.get_or_create(**data)

    @staticmethod
    def create(data):
        return Book.objects.create(**data)

    @staticmethod
    def delete_all():
        return Book.objects.all().delete()
