import json
import logging
import requests

from django.conf import settings
from django.core.management.base import BaseCommand

from books.services import AuthorService, BookService


logger = logging.getLogger(__name__)

# TODO:
# Ideally this management command would be ran using a celery task
# at the moment it rans every single time dev's start docker-compose.
# The reason behind this decision was to reduce the amount of actions needed
# to run this project.


class Command(BaseCommand):
    help = 'Fetches books data from external endpoint.'

    def handle(self, *args, **options):
        response = requests.get(settings.BOOKS_LIST_ENDPOINT)

        if response.status_code == 200:
            try:
                data = json.loads(response.content)
            except ValueError as e:
                # ideally here I would send an email to admin
                # notifying them with the message exception
                logger.error(e)
                return

            for book in data['books']:
                try:
                    author_data = {'name': book['author']}
                except KeyError as e:
                    # ideally here I would also send an email to admin
                    # notifying them with the message exception
                    logger.error(e)
                    return

                try:
                    book_data = {
                        'name': book['name'],
                        'isbn': book['isbn'],
                        'published_at': book['published_at'],
                        'cover': book['cover']
                    }
                except KeyError as e:
                    # ideally here I would also send an email to admin
                    # notifying them with the message exception
                    logger.error(e)
                    return

                # TODO:
                # Currently if a book (or author) as a misspealed name, there
                # is no way to update it. A new service needs to be created
                # in order to update fields of existing books or create
                # a book (BookService.update_or_create(book_data))
                author, author_created = AuthorService.get_or_create(author_data)
                logger.info('Author: {} CREATED: {}'.format(author.name, author_created))

                book, book_created = BookService.get_or_create(book_data)
                if book_created:
                    # TODO: implement a BookService to add relationships
                    # and to save
                    book.authors.add(author)
                    book.save()
                logger.info('Book: {} CREATED: {}'.format(book.name, book_created))
        else:
            # ideally here I would also send an email to admins notifying them
            logger.warning('URL: {} has returned status_code: {}'.format(
                settings.BOOKS_LIST_ENDPOINT,
                response.status_code
            ))
            return
