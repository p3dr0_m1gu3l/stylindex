from django.shortcuts import render
from .services import AuthorService, BookService


def order_values_validation(order):
    valid_order_options = ['name', '-name', 'published_at', '-published_at']
    if order in valid_order_options:
        return order
    return None


def books(request):
    """
    Render view with all books.

    Accepts ordering (ascending and descending) for
    book name and publish date fields only.

    Orders books by 'id' (default). This means that
    if a user tries to pass an order value other than
    'name' or 'published_at' directly through
    the url, ignores that order request and render books
    ordered by the default value 'id'.
    """
    order = request.GET.get('order', None)
    order = order_values_validation(order)
    books_qs = BookService.get_all()

    if order:
        books_qs = books_qs.order_by(order)
    context = {'books': books_qs}

    return render(request, 'books/books.html', context)


def authors(request):
    """
    Render view with all authors and all books
    associated with each author, example:

    Author: John
    Books: "Wunsch ThurmanVille", "Hintz LeathaVille"
    """
    authors = AuthorService.get_all()
    context = {'authors': authors}

    return render(request, 'books/authors.html', context)
