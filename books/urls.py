from django.conf.urls import url

from .views import authors, books


app_name = "books"
urlpatterns = [
    url(r"^books/$", books, name="books"),
    url(r"^authors/$", authors, name="authors")
]
