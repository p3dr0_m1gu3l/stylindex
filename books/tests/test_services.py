import datetime
import unittest

from mock import Mock, patch

from ..models import Author, Book
from ..services import AuthorService, BookService


# TODO:
#
# * Add test for AuthorService.create();
# * Add test for AuthorService.delete_all();
# * Add test for BookService.create();
# * Add test for BookService.delete_all()


class TestAuthorServices(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        # create a author service instance
        cls.author_service = AuthorService()

        # define dummy data
        cls.data1 = {'name': 'Mario'}
        cls.data2 = {'name': 'Luigi'}

        # create mock author objects
        cls.author_object1 = Mock(spec=Author)
        cls.author_object1.configure_mock(id=4, **cls.data1)

        cls.author_object2 = Mock(spec=Author)
        cls.author_object2.configure_mock(id=6, **cls.data2)

    @patch('books.services.AuthorService.get_or_create')
    def test_authorservice_creates_new_author_object(self, mock_authorservice_get_or_create):
        # mock the return value when calling AuthorService.get_or_create()
        mock_authorservice_get_or_create.return_value = (self.author_object1, True)
        author, created = self.author_service.get_or_create(self.data1)

        self.assertTrue(created)
        self.assertEqual(author.name, self.author_object1.name)
        self.assertEqual(author.id, self.author_object1.id)

    @patch('books.services.AuthorService.get_or_create')
    def test_authorservice_returns_author_object(self, mock_authorservice_get_or_create):
        # mock the return value when calling AuthorService.get_or_create()
        mock_authorservice_get_or_create.return_value = (self.author_object1, False)
        author, created = self.author_service.get_or_create(self.data1)

        self.assertFalse(created)
        self.assertEqual(author.name, self.author_object1.name)
        self.assertEqual(author.id, self.author_object1.id)

    @patch('books.services.AuthorService.get_all')
    def test_authorservice_returns_all_objects(self, mock_authorservice_get_all):
        mock_authorservice_get_all.return_value = [self.author_object1, self.author_object2]
        authors = self.author_service.get_all()

        self.assertEqual(len(authors), 2)
        self.assertEqual(authors[0], self.author_object1)
        self.assertEqual(authors[1], self.author_object2)


class TestBookServices(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        # create a book service instance
        cls.book_service = BookService()

        # define dummy data
        cls.data1 = {
            'name': 'The President is Missing',
            'isbn': '3181444340',
            'published_at': datetime.date(2018, 1, 3),
            'cover': 'https://lorempixel.com/640/480/?82539'
        }
        cls.data2 = {
            'name': 'Educated',
            'isbn': '1183244689',
            'published_at': datetime.date(2013, 2, 5),
            'cover': 'https://lorempixel.com/640/480/?82539'
        }

        # create mock book objects
        cls.book_object1 = Mock(spec=Book)
        cls.book_object1.configure_mock(id=2, **cls.data1)

        cls.book_object2 = Mock(spec=Book)
        cls.book_object2.configure_mock(id=5, **cls.data2)

    @patch('books.services.BookService.get_or_create')
    def test_bookservice_creates_new_book_object(self, mock_bookservice_get_or_create):
        # mock the return value when calling BookService.get_or_create()
        mock_bookservice_get_or_create.return_value = (self.book_object1, True)
        book, created = self.book_service.get_or_create(self.data1)

        self.assertTrue(created)
        self.assertEqual(book.name, self.book_object1.name)
        self.assertEqual(book.id, self.book_object1.id)

    @patch('books.services.BookService.get_or_create')
    def test_bookservice_returns_author_object(self, mock_bookservice_get_or_create):
        # mock the return value when calling BookService.get_or_create()
        mock_bookservice_get_or_create.return_value = (self.book_object1, False)
        book, created = self.book_service.get_or_create(self.data1)

        self.assertFalse(created)
        self.assertEqual(book.name, self.book_object1.name)
        self.assertEqual(book.id, self.book_object1.id)

    @patch('books.services.BookService.get_all')
    def test_bookservice_returns_all_objects(self, mock_bookservice_get_all):
        mock_bookservice_get_all.return_value = [self.book_object1, self.book_object2]
        books = self.book_service.get_all()

        self.assertEqual(len(books), 2)
        self.assertEqual(books[0], self.book_object1)
        self.assertEqual(books[1], self.book_object2)
