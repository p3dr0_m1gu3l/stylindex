import json
from mock import Mock, call, patch
from requests.exceptions import HTTPError

from django.conf import settings
from django.test import TestCase

from ..management.commands.fetch_books_data import Command
from ..models import Author, Book


# TODO:
#
# * Add test to trigger ValueError exception when no JSON object
#   can be decoded;
# * Add tests to trigger KeyError exception when book keys do not
#   match the expected


class TestCommands(TestCase):

    @classmethod
    def setUpTestData(cls):
        # define dummy data
        cls.data = {
            "books": [
                {
                    "book_id": 1,
                    "name": "Becker West Arnoldo",
                    "isbn": "3181444340",
                    "published_at": "2000-01-01",
                    "author": "Mrs. John Doe",
                    "cover": "https://lorempixel.com/640/480/?82539"
                }
            ]
        }
        cls.wrong_data = {
            "books": [
                {
                    "book_id": 1,
                    "name": "Becker West Arnoldo",
                    "isbn": "3181444340",
                    "published_at": "2000-01-01",
                    "autor": "Mrs. John Doe",
                    "cover": "https://lorempixel.com/640/480/?82539"
                }
            ]
        }
        # create mock author objects
        cls.author_object1 = Mock(spec=Author)
        cls.author_object1.configure_mock(id=1, name=cls.data['books'][0]['author'])
        # create mock book objects
        cls.book_object1 = Mock(spec=Book)
        cls.book_object1.configure_mock(
            id=1,
            name=cls.data['books'][0]['name'],
            isbn=cls.data['books'][0]['isbn'],
            published_at=cls.data['books'][0]['published_at'],
            cover=cls.data['books'][0]['cover']
        )

    def _mock_response(
            self,
            status=200,
            content="",
            json_data=None,
            raise_for_status=None
    ):
        """
        Helper function to build mock responses.
        """
        mock_resp = Mock()
        # mock raise_for_status call w/optional error
        mock_resp.raise_for_status = Mock()
        if raise_for_status:
            mock_resp.raise_for_status.side_effect = raise_for_status
        # set status code and content
        mock_resp.status_code = status
        mock_resp.content = content
        # add json data if provided
        if json_data:
            mock_resp.json = Mock(
                return_value=json_data
            )
        return mock_resp

    @patch('requests.get')
    @patch('books.management.commands.fetch_books_data.logger')
    def test_books_data_command_returns_500(self, mock_logging, mock_get):
        response = self._mock_response(status=500, raise_for_status=HTTPError('Amazon S3 is down'))
        mock_get.return_value = response
        cmd = Command()
        cmd.handle()

        mock_logging.warning.assert_called_with('URL: {} has returned status_code: {}'.format(
            settings.BOOKS_LIST_ENDPOINT,
            response.status_code
        ))

    @patch('requests.get')
    @patch('books.services.AuthorService.get_or_create')
    @patch('books.services.BookService.get_or_create')
    @patch('books.management.commands.fetch_books_data.logger')
    def test_books_data_command_processes_data(
        self,
        mock_logging,
        mock_bookservice_get_or_create,
        mock_authorservice_get_or_create,
        mock_get
    ):
        response = self._mock_response(status=200, content=json.dumps(self.data))
        mock_get.return_value = response
        mock_authorservice_get_or_create.return_value = (self.author_object1, True)
        mock_bookservice_get_or_create.return_value = (self.book_object1, True)
        cmd = Command()
        cmd.handle()

        log_info_messages = [
            call('Author: Mrs. John Doe CREATED: True'),
            call('Book: Becker West Arnoldo CREATED: True')
        ]
        mock_logging.info.assert_has_calls(log_info_messages)

    @patch('requests.get')
    @patch('books.services.AuthorService.get_or_create')
    @patch('books.services.BookService.get_or_create')
    @patch('books.management.commands.fetch_books_data.logger')
    def test_books_data_command_with_wrong_author_data_triggers_execption(
        self,
        mock_logging,
        mock_bookservice_get_or_create,
        mock_authorservice_get_or_create,
        mock_get
    ):
        response = self._mock_response(status=200, content=json.dumps(self.wrong_data))
        mock_get.return_value = response
        mock_authorservice_get_or_create.side_effect = KeyError('author',)
        mock_bookservice_get_or_create.return_value = (self.book_object1, True)
        cmd = Command()
        cmd.handle()

        self.assertTrue(mock_logging.error.called)
