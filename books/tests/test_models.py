import datetime

from django.test import TestCase

from ..models import Author, Book


class TestAuthorModels(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.author = Author.objects.create(name='Mrs. John Doe')

    @classmethod
    def tearDownClass(cls):
        Author.objects.all().delete()
        super().tearDownClass()

    def test_author_name_is_the_object_name(self):
        self.assertEqual(self.author.__str__(), self.author.name)


class TestBookModels(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.author = Author.objects.create(name='Mrs. John Doe')
        cls.book = Book.objects.create(
            name='Becker West Arnoldo',
            isbn='3181444340',
            published_at=datetime.date(2000, 1, 1),
            cover='https://lorempixel.com/640/480/?82539'
        )
        cls.book.authors.add(cls.author)
        cls.book.save()

    @classmethod
    def tearDownClass(cls):
        Book.objects.all().delete()
        Author.objects.all().delete()
        super().tearDownClass()

    def test_book_name_is_the_object_name(self):
        self.assertEqual(self.book.__str__(), self.book.name)
