import datetime

from django.test import TestCase
from django.urls import reverse

from ..services import AuthorService, BookService


class TestAuthorsView(TestCase):

    @classmethod
    def setUpTestData(cls):
        # define dummy data
        cls.author1 = AuthorService.create({'name': 'Mario'})
        cls.author2 = AuthorService.create({'name': 'Luigi'})

    @classmethod
    def tearDownClass(cls):
        AuthorService.delete_all()
        super().tearDownClass()

    def test_authors_endpoint_returns_200(self):
        response = self.client.get(reverse('books:authors'))
        self.assertEqual(response.status_code, 200)

    def test_authors_endpoint_returns_all_authors(self):
        response = self.client.get(reverse('books:authors'))
        self.assertEqual(len(response.context['authors']), 2)
        self.assertEqual(response.context['authors'][0], self.author1)


class TestBooksView(TestCase):

    @classmethod
    def setUpTestData(cls):
        # define dummy data
        cls.author1 = AuthorService.create({'name': 'Mario'})
        cls.author2 = AuthorService.create({'name': 'Luigi'})
        cls.book1 = BookService.create(
            {
                'name': 'The President is Missing',
                'isbn': '3181444340',
                'published_at': datetime.date(2018, 1, 3),
                'cover': 'https://lorempixel.com/640/480/?82539'
            }
        )
        cls.book2 = BookService.create(
            {
                'name': 'Educated',
                'isbn': '1183244689',
                'published_at': datetime.date(2013, 2, 5),
                'cover': 'https://lorempixel.com/640/480/?82539'
            }
        )
        # create many to many relationship between books and authors
        cls.book1.authors.add(cls.author1)
        cls.book2.authors.add(cls.author2)
        cls.book1.save()
        cls.book2.save()

    @classmethod
    def tearDownClass(cls):
        BookService.delete_all()
        AuthorService.delete_all()
        super().tearDownClass()

    def test_books_endpoint_returns_200(self):
        response = self.client.get(reverse('books:books'))
        self.assertEqual(response.status_code, 200)

    def test_books_endpoint_returns_all_books(self):
        response = self.client.get(reverse('books:books'))
        self.assertEqual(len(response.context['books']), 2)
        self.assertEqual(response.context['books'][0], self.book1)

    def test_books_endpoint_returns_all_books_ordered_by_name_asc(self):
        response = self.client.get(reverse('books:books') + '?order=name')
        self.assertEqual(response.context['books'][0].name, self.book2.name)
        self.assertEqual(response.context['books'][1].name, self.book1.name)

    def test_books_endpoint_returns_all_books_ordered_by_name_desc(self):
        response = self.client.get(reverse('books:books') + '?order=-name')
        self.assertEqual(response.context['books'][0].name, self.book1.name)
        self.assertEqual(response.context['books'][1].name, self.book2.name)

    def test_books_endpoint_returns_all_books_ordered_by_published_date_asc(self):
        response = self.client.get(reverse('books:books') + '?order=published_at')
        self.assertEqual(response.context['books'][0].name, self.book2.name)
        self.assertEqual(response.context['books'][1].name, self.book1.name)

    def test_books_endpoint_returns_all_books_ordered_by_published_date_desc(self):
        response = self.client.get(reverse('books:books') + '?order=-published_at')
        self.assertEqual(response.context['books'][0].name, self.book1.name)
        self.assertEqual(response.context['books'][1].name, self.book2.name)

    def test_wrong_order_params_returns_all_books_ordered_by_id_asc(self):
        response = self.client.get(reverse('books:books') + '?order=superparam')
        self.assertEqual(response.context['books'][0].name, self.book1.name)
        self.assertEqual(response.context['books'][1].name, self.book2.name)
