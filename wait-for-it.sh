#!/bin/bash

set -e
host="$1"      # stylindexdb
user="$2"      # user
pass="$3"      # test
database="$4"  # stylindex
shift

until PGPASSWORD="$pass" psql -h "$host" -U "$user" "$database" -c '\q'; do
    >&2 echo "Postgres is unavailable - sleeping"
    sleep 1
done

# mysql is now accepting connections
>&2 echo "Postgres is up - executing command"

# migrate migrations
python manage.py migrate

# fetch books data
python manage.py fetch_books_data

# start django
python manage.py runserver 0.0.0.0:8000
