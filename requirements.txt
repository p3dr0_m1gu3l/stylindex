Django==1.11.14
dj-database-url==0.4.2
psycopg2==2.7.5
requests==2.19.1
coverage==4.5.1
mock==2.0.0
