# Stylindex

Stylindex is a django project that simply collects data from an external endpoint, stores the data in a postgres database and displays it to the user.

This project uses the django framework give its simple setup and use a management command to collect and store the data into a postgres database. The reason for this was because books are usually not updated that often (unless there is a typo).

Also it didn't made since to hit the external endpoint everytime a user wants to see the data, and since each book had their own unique 'ISBN' number, storing the data in a database and update the database once a week or so, would be an easy task.

Additionally, on a later stage we could even create a celery task to fetch the data twice a week and update the database in case there are new books, or a book that needs to be udpated (misspelled title).

# Run project

1 - [Install docker](https://docs.docker.com/) and [Docker Compose](https://docs.docker.com/compose/install/) on your machine

2 - Clone this repo with `git clone git@github.com:p3dr0migue1/stylindex.git`

3 - Create a `.env` file inside the project directory and add the the following `key=values`:
```bash
DATABASE_URL=postgres://user:test@stylindexdb:5432/stylindex
```

4 - Run `docker-compose up --build` this will:
- setup the docker containers;
- install the project requirements;
- setup postgres database;
- run django migrations;
- run the management command `fetch_books_data`;
- and start the django server

5 - Once everything is installed and running visit: http://localhost:8000/books/

## Run tests (from outside the container)

### All tests
```bash
docker-compose run --rm stylindexweb python manage.py test
```

### Specific tests
```bash
docker-compose run --rm stylindexweb python manage.py books.tests.test_services.TestAuthorServices.test_authorservice_creates_new_author_object
```

### Coverage tests
```bash
docker-compose run --rm stylindexweb coverage run manage.py test books
```

### Coverage to html
```bash
docker-compose run --rm stylindexweb coverage html
```

## Run tests (from within the container)

### Go into the container
```bash
docker-compose run --rm stylindexweb bash
```

### All tests
```bash
python manage.py test
```

### Specific tests
```bash
python manage.py books.tests.test_services.TestAuthorServices.test_authorservice_creates_new_author_object
```

### Coverage tests
```bash
coverage run manage.py test books
```

### Coverage to html
```bash
coverage html
```
